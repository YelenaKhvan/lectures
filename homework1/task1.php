<?php

//1
$number = -5;

if ($number < 0) {
    echo "Число $number отрицательное.";
} else {
    echo "Число $number не отрицательное.";
}

//2
$string = 'hello';
echo strlen($string);

//3
$word = "my granny";
$letter = "a";

if (strpos($word, $letter) !== false) {
    echo "да"; // Буква 'a' найдена в строке "my granny"
} else {
    echo "нет"; // Буква 'a' не найдена в строке "my granny"
}

//4
$num = 90;

if ($num % 2 == 0 && $num % 3 == 0 && $num % 5 == 0 && $num % 6 == 0 && $num % 9 == 0) {
    echo $num;
} else {
    echo "число не найдено";
}

//5
$num = 100;

if ($num % 3 == 0 && $num % 5 == 0 && $num % 7 == 0 && $num % 11 == 0) {
    echo $num;
} else {
    echo "число не найдено";
}

//6
$str = "string";
$last = $str[strlen($str) - 1];
echo "последний символ: " . $last;

//7 S= 0,5*b*h
$base = 10; 
$height = 5;

$area = 0.5 * $base * $height;

echo "Площадь треугольника: " . $area;


//8 S= a*b
$a = 10; 
$b = 5;   

$area = $a * $b;

echo "Площадь прямоугольника: " . $area;

//9
$num = 5;

$square = $num * $num;

echo "Квадрат числа $num: " . $square;
