<?php

$students = 11;
$teachers = 1;
// && - and false && true // false true && true // true false && false // false
// || - or false || true // true  false || false // false
// ! - not

//if ($students == 10 && $teachers == 1) {// 10 == 10 // true 0 == 1 // false
//    echo "All is good";
//} else {
//    echo "Something is wrong";
//}

//if ($students == 10 || $teachers == 1) {// 10 == 10 // true 0 == 1 // false
//    echo "All is good";
//} else {
//    echo "Something is wrong";
//}
if ( ! ($students == 10 & $teachers == 1)) {
    //11 == 10 // false
    //1 == 1 //true
//   ! false
//    true
    echo "All is good" . PHP_EOL;
    echo "All is good";
} else {
    echo "Something is wrong";
}


?>